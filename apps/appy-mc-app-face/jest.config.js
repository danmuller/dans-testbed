module.exports = {
  name: 'appy-mc-app-face',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/appy-mc-app-face',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
